variable "account" {
  description = "The AWS account number"
}

variable "account_name" {
  description = "The AWS account name or alias"
}

variable "env" {
  description = "The environment name"
}

variable "elc_id" {
  description = "The AWS elc instance id"
}

variable "elc_name" {
  description = "The AWS elc instance name"
}

variable "alarm_actions" {
  type = "list"
  description = "The AWS Cloud Watch alarm actions"
}

// CPU Utilization
variable "cpu_utilization_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "cpu_utilization_evaluation_periods" {
  default = 3
}

variable "cpu_utilization_period" {
  default = 300
}

variable "cpu_utilization_statistic" {
  default = "Average"
}

variable "cpu_utilization_threshold" {
  default = 90
}

variable "cpu_utilization_unit" {
  default = ""
}

// Evictions
variable "evictions_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "evictions_evaluation_periods" {
  default = 3
}

variable "evictions_period" {
  default = 300
}

variable "evictions_statistic" {
  default = "Average"
}

variable "evictions_threshold" {
  default = 0
}

variable "evictions_unit" {
  default = ""
}

// Swap Usage
variable "swap_usage_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "swap_usage_evaluation_periods" {
  default = 3
}

variable "swap_usage_period" {
  default = 300
}

variable "swap_usage_statistic" {
  default = "Average"
}

variable "swap_usage_threshold" {
  default = 52428800
}

variable "swap_usage_unit" {
  default = ""
}
