// VPC
module "test_vpc" {
  source  = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.8"
  account = "${var.account}"

  name                      = "cloudwatch_test_elc"
  domain_name               = "cloudwatch_test_elc.local"
  cidr_block                = "10.0.0.0/16"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = 4
  hosted_zone_comment       = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

module "elc" {
  source  = "git::https://bitbucket.org/credibilit/terraform-elasticache-redis-blueprint.git?ref=0.0.2"
  account = "${var.account}"

  name            = "redis-test-acme"
  vpc_id          = "${module.test_vpc.vpc}"
  subnet_ids      = [
    "${module.test_vpc.public_subnets}"
  ]
  tags            = {
    Foo = "bar"
  }
}

// Action
resource "aws_sns_topic" "monitoring" {
  name         = "monitoring"
  display_name = "monitoring"
}

module "test_elc_cloudwatch" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  elc_id        = "${module.elc.elasticache_replication_group["id"]}"
  elc_name      = "redis-test-acme"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]
}
