# Swap Usage
resource "aws_cloudwatch_metric_alarm" "swap_usage" {
  alarm_name          = "[${var.account_name}] [elc] [${var.env}] ${var.elc_name} - SwapUsage"
  comparison_operator = "${var.swap_usage_comparison_operator}"
  evaluation_periods  = "${var.swap_usage_evaluation_periods}"
  metric_name         = "SwapUsage"
  namespace           = "AWS/ElastiCache"
  period              = "${var.swap_usage_period}"
  statistic           = "${var.swap_usage_statistic}"
  threshold           = "${var.swap_usage_threshold}"
  unit                = "${var.swap_usage_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    CacheClusterId = "${var.elc_name}"
  }
}

output "swap_usage" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.swap_usage.id}"
  }
}
